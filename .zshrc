# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="robbyrussell"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git vi-mode osx)

source $ZSH/oh-my-zsh.sh

# User configuration

export PATH="/usr/local/Cellar/php56/5.6.31_7/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/local/bin:/usr/local/mysql/bin:/Users/tony/.rvm/gems/ruby-2.0.0-p247/bin:/Users/tony/.rvm/gems/ruby-2.0.0-p247@global/bin:/Users/tony/.rvm/rubies/ruby-2.0.0-p247/bin:/Users/tony/.rvm/bin:/Library/WebServer/Documents/cake_php/lib/Cake/Console"
# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Added by Tony
setopt appendhistory autocd beep extendedglob nomatch notify
setopt hist_ignore_dups share_history inc_append_history extended_history

# http://stackoverflow.com/questions/19961239/pelican-3-3-pelican-quickstart-error-valueerror-unknown-locale-utf-8
# For pelican 
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

alias la='ls -lah'
alias latr='ls -lahtr'
alias lah='ls -lah'
alias ..='cd ..'
alias gc='git commit'
alias gl='git log'
alias gs='git status'
alias gaa='git add --all'
alias cpwd="pwd | tr -d '\n' | pbcopy"
alias gk="geeknote"
alias vgs='vagrant global-status'
alias vup='vagrant up'
alias vhalt='vagrant halt'
alias vssh='vagrant ssh'
# This will print a tree of the current folder
alias tree='find . -print | sed -e "s;[^/]*/;|____;g;s;____|; |;g"'
# Make the Grep output always in color.
export GREP_OPTIONS='--color=always'
# Aliases for Tmux
alias tma='tmux attach -t '
alias tml='tmux list-sessions'
alias tmn='tmux -2 new -s '

# alias vim to the mac vim 
alias vim='/Applications/MacVim.app/Contents/MacOS/Vim'

# These are from the bashrc
PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
ANT_HOME=/Users/tonygeorge/apache-ant-1.9.3
PATH=$PATH:$ANT_HOME/bin
PATH=$PATH:/usr/local/bin/phpunit
# End of bashrc copied items.
# These are from bash_profile
export CLICOLOR=1
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
export PATH=/Users/tony/.rvm/gems/ruby-2.0.0-p247/bin:/Users/tony/.rvm/gems/ruby-2.0.0-p247@global/bin:/Users/tony/.rvm/rubies/ruby-2.0.0-p247/bin:/Users/tony/.rvm/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/local/mysql/bin

export PATH="$PATH:/Library/WebServer/Documents/cake_php/lib/Cake/Console"
export PATH="$PATH:/Users/tonygeorge/.composer/vendor/bin"

alias ct='ctags-exuberant'

export PATH="/usr/local/mysql/bin:$PATH"
export PATH="/opt/local/bin:$PATH"
export PATH="/usr/local/go/bin:$PATH"

# End of items copied from bash_profile.

# Source the goto functions
source "$HOME/dotfiles/zsh_goto_functions.sh"

# The following lines were added by compinstall
zstyle :compinstall filename '/Users/tonygeorge/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Enable Ctrl-R for reverse history search
bindkey "^R" history-incremental-search-backward
export PATH="/usr/local/bin:$PATH"
export EDITOR='/Applications/MacVim.app/Contents/MacOS/Vim'

# Path for Anaconda.
export PATH="/Users/tonygeorge/anaconda/bin":$PATH

# Add ssh identity so that Vagrant can use this for agent forwarding.
ssh-add ~/.ssh/id_rsa

