#Tony added the following to set some aliases.
alias la='ls -lah'
alias lah='ls -lah'
alias ..='cd ..'
alias gc='git commit'
alias gl='git log'
alias gs='git status'
alias gaa='git add --all'

# This will print a tree of the current folder
alias tree='find . -print | sed -e "s;[^/]*/;|____;g;s;____|; |;g"'

# Make the Grep output always in color.
export GREP_OPTIONS='--color=always'

# Aliases for Tmux
alias tma='tmux attach -t '
alias tml='tmux list-sessions'
alias tmn='tmux -2 new -s '

# Increases size of the bash history file
HISTSIZE=1000
