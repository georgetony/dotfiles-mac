" Disable audible bell.
set visualbell t_vb=

set nocompatible
set encoding=utf8

" Set line number on.
" set number
" Set relative number on
set rnu
" Make Space the leade
let mapleader = ","

" Don't close buffers when switching. Preserve history and changes.
set hidden

" Quickly edit / reload the vimrc file
"nmap <silent> <Leader>ev :e $MYVIMRC<CR>
"nmap <silent> <Leader>sv :so $MYVIMRC<CR>

" Don't need backups
set nobackup

" Disable the swap file. Enable this before loading large files.
set noswapfile

" Disable the normal behavior of deleting current file and writing it again.
set nowritebackup

" Make Space-e same as Explore
nmap <Leader>e <c-p>

" Map n to nzz so the next found will be brought to the center of screen.
map n nzz

" Map F4 to :buffers.
noremap <F4> <Esc>:buffers<CR>:buffer<Space>

" Map <Leader>w to :w<Enter>
map <Leader>w :w<CR>
imap ,w <Esc>:w<CR>


" Mapping for more window navigation
map <Leader>h <c-w>h
map <Leader>j <c-w>j
map <Leader>k <c-w>k
map <Leader>l <c-w>l

" Resizing the window
" Equalize windows
" map <Leader>w= <c-w>=
" Increase / Decrease width of the current window
" map <Leader>wh <c-w>>
" map <Leader>wl <c-w><
" Increase / Decrease height of the current window
" map <Leader>wk <c-w>+
" map <Leader>wj <c-w>-

nnoremap <c-j> :resize +2<CR>
nnoremap <c-k> :resize -2<CR>
nnoremap <c-h> :vertical resize -2<CR>
nnoremap <c-l> :vertical resize +2<CR>


" Map <Leader>t to gT this will cycle through the tabs.
map <Leader>t gT

" Map <F3> to work like switching between two buffers.
" Same as entering <Leader>l and then # followed by CR
:imap <F3> <Esc>:bu#<CR>
:map <F3> :bu#<CR>

" Map <F8> to work like save button.
map <F8> :w<CR>
imap <F8> <Esc>:w<CR>

" Map <F6> to work like i <CR> <Esc>
:map <F6> i<CR><Esc>

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" Set the tabstop, shiftwidth etc
set tabstop=2
"set softtabstop=2
set shiftwidth=2
set smarttab
set expandtab

" Mapping for toggle invisible characters
nmap <Leader>1 :set list!<CR>
set listchars=tab:>-,trail:.,extends:>

"Invisible character colors
highlight NonText guifg=#E0DADC
highlight SpecialKey guifg=#E0DADC

" Set file type for .ctp files. This will enable the HTML and PHP snippets
au BufRead *.ctp set ft=php.cakephp.html

" Set file type for PHP files. This will help to enable the HTML snippets.
au BufRead *.php set ft=php.cakephp.html

" Set the font 
" Menlo Bold:h12
set guifont=Menlo\ Bold:h13

" Open all code folds
set foldlevel=99

" Set a vertical line at 80th col.
hi ColorColumn ctermbg=LightGrey
set colorcolumn=80 


" Incremental search
set incsearch

" Start Pathogen
"execute pathogen#infect()
"
"Start Vim Plug: https://github.com/junegunn/vim-plug
call plug#begin('~/.vim/plugged')
Plug 'ajh17/Spacegray.vim'
Plug 'ryanoasis/vim-devicons'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdcommenter'
Plug 'vim-syntastic/syntastic'
Plug 'ervandew/supertab'
Plug 'Valloric/YouCompleteMe'
Plug 'jiangmiao/auto-pairs'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'vim-scripts/functionlist.vim'
Plug 'https://github.com/adelarsq/vim-matchit'
Plug 'gregsexton/MatchTag'
call plug#end()



" Settings for syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" Set to passive mode. Run SyntasticCheck to check for errors
let g:syntastic_mode_map = { 'mode': 'passive' }

" NERDTree mappings
map <C-n> :NERDTreeToggle<CR>
" Show hidden files by default
let NERDTreeShowHidden=1

syntax on
filetype plugin indent on
set omnifunc=syntaxcomplete

" Turn wrap off. 
set wrap!

" Toggle the function list
map <F2> :Flisttoggle<CR>

" Paste mode toggle.
set pastetoggle=<F2>


" Arduino Filetype
au BufRead,BufNewFile *.pde set filetype=arduino
au BufRead,BufNewFile *.ino set filetype=arduino

" Map <F7> to goto prev buffer and delete the current buffer from memory.
nmap <F7> :bp\|bd #<CR>
imap <F7> <Esc>:bp\|bd #<CR>

" Remap ; to :. This eliminates the need to press SHIFT when saving 
" and other operations.
nnoremap ; :

" Map <Leader>m to jump to the matching brace
map <Leader>m %
vmap <Leader>m %

" ignore list for CtrlP
" This will speed it up.
let g:ctrlp_custom_ignore = 'tmp\|DS_Store\|report_files\|report_templates\|git\|img\|icons\|Report Templates\|db_clean_up\|PHPExcel_working'

" Always show status line
set laststatus=2                             " always show statusbar  

hi StatusLine ctermbg=white ctermfg=black guifg=black guibg=white


" Status Line {  
set statusline=  
set statusline+=\[\b\:\ %n\]				" buffer number  
set statusline+=\ %f\                        " filename   
set statusline +=%y                          " Filetype
set statusline +=%m                          " Show [+] when the buffer is modified
set statusline+=%*       "switch back to normal statusline highlight
set statusline+=%=                           " right align remainder  
set statusline+=%-14(%l,%c%V%)               " line number and character 
set statusline+=%<%P                         " file position  
set statusline +=\ [%=%L\ lines]	           " Show total number of lines
"}

" In insert mode, F5 to the same as shift enter in the GUI mode
imap <F5> <CR><Esc>O
"
" In insert mode, add semicolon at the next line end.
inoremap ;; <DOWN><END>;
inoremap ;;k <DOWN><END>;<UP>
inoremap .. <DOWN><END>.
inoremap ,, <DOWN><END>,

map <Space> viw

" Auto continue comments
au Bufenter *.php set comments=sl:/*,mb:*,elx:*/
set formatoptions+=r

" Insert a ; at the end of the current line and go to new line
imap <F11> <C-o>A;

" Goto the end of the current line, insert a comma, and press enter.
nmap OO $a,<CR>

" Map .. to -> for PHP development
imap <Leader>. ->

" Map ,e to move cursor to end of line in insert mode.
imap <Leader>e <ESC>g_a

let g:S = 0  "result in global variable S
function! Sum(number)
	let g:S = g:S + a:number
	return a:number
endfunction

"Clear the current search term.
function! ClearSearch()
	let @/ = ""
endfunction

" Session Management
let g:session_directory = "~/.vim/session"
let g:session_autoload = "no"
let g:session_autosave = "yes"
let g:session_command_aliases = 1

nnoremap <Leader>so :OpenSession
nnoremap <Leader>ss :SaveSession
nnoremap <Leader>sd :DeleteSession<CR>
nnoremap <Leader>sc :CloseSession<CR>

" Mappina <F9> to :SessionOpen
map <F9> :OpenSession<CR>
" Mapping <F10> to :SessionClose and :q
map <F10> :SaveSession<CR>:CloseSession<CR>:q<CR>

" Search and replace the current word.
nnoremap <Leader>s :%s/\<<C-r><C-w>\>/

" Macros that are saved.
let @c = 'ci(kVp==V:s/\,\ /\,\r/gvi(='
let @b = '0/_xxx%%bblldt)x'
" This macro, when ran at the beginning of an array (PHP) line, will 
" split the array( and its contents and ) into 3 separate lines and press ==.
let @y = '0/arraywwci(�k5Vp=='
"This macro when ran inside an array(...) (PHP) will replace all comma and 
"space by a comma and new line.
let @x = 'V:s/\,\ /\,\r/gvi(=])2j'


" Highlighting current line.
set cursorline
hi CursorLine cterm=none ctermbg=LightGrey ctermfg=None

set clipboard=unnamed


" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"
" Private snippets directory. Use full path
let g:UltiSnipsSnippetDirectories = ['/Users/tonygeorge/dotfiles/.vim/my_snippets']

" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

" Show keys being pressed in normal mode.
set showcmd
