#!/bin/zsh
goto_read(){
	cd /Library/WebServer/Documents/read
	pwd
}
goto_webroot(){
	cd /Library/WebServer/Documents
	pwd
}

goto_custom(){
	cd /Library/WebServer/Documents/custom
	pwd
}
goto_exp(){
	cd /Library/WebServer/Documents/expense
	pwd
}

goto_toeic(){
	cd /Library/WebServer/Documents/toeic
	pwd
}

goto_baseproject(){
	cd /Library/WebServer/Documents/baseproject
	pwd
}

goto_vfc(){
	cd /Library/WebServer/Documents/vfc
	pwd
}

goto_pbt.btgocm.com(){
	cd /Library/WebServer/Documents/pbt.btgocm.com
	pwd
}
goto_gst.btgocm.com(){
	cd /Library/WebServer/Documents/gst.btgocm.com
	pwd
}
goto_wer1(){
	cd /Library/WebServer/Documents/wer1/
	pwd
}
goto_bmi2(){
	cd /Library/WebServer/Documents/bmi2
	pwd
}

goto_mvs(){
	cd /Library/WebServer/Documents/mvs
	pwd
}

goto_proage(){
	cd /Library/WebServer/Documents/proage
	pwd
}

goto_cake_php(){
	cd /Library/WebServer/Documents/cake_php
	pwd
}

goto_baseproject(){
	cd /Library/WebServer/Documents/baseproject
	pwd
}

goto_ocm(){
	cd /Library/WebServer/Documents/ocm.asia
	pwd
}
goto_smatra(){
	cd /Library/WebServer/Documents/smatra/
	pwd
}
goto_bizreports(){
	cd /Library/WebServer/Documents/bizreports.org/
	pwd
}
goto_bsec(){
	cd /Library/WebServer/Documents/bsec.info/
	pwd
}
goto_invoices(){
	cd /Users/tonygeorge/Documents/CUSTOMSOFTWARE_PTE_LTD/Invoices
	pwd
}
goto_vimbundle(){
	cd /Users/tonygeorge/dotfiles/.vim/bundle/
	pwd
}
goto_eeg_mindwave(){
	cd /Users/tonygeorge/Documents/Python/eeg_mindwave
	pwd
}

goto_bci_ml(){
	cd /Users/tonygeorge/Documents/Python/bci_text_ml/data_playground/Tony_George_Emotiv/
	pwd
}

goto_jetdrine(){
	cd /Volumes/JetDrive/
	pwd
}

goto_vagrant(){
	cd ~/Vagrant_VMs/
	pwd
}
