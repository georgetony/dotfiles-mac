#Tony added the following to set some aliases.
alias la='ls -lah'
alias lah='ls -lah'
alias ..='cd ..'
alias gc='git commit'
alias gl='git log'
alias gs='git status'
alias gaa='git add --all'

# This will print a tree of the current folder
alias tree='find . -print | sed -e "s;[^/]*/;|____;g;s;____|; |;g"'

# Adding the new path to vim

